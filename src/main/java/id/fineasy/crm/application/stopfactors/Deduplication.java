package id.fineasy.crm.application.stopfactors;

import id.fineasy.crm.application.component.ConfigUtils;
import id.fineasy.crm.application.component.DataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class Deduplication {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    DataUtils dataUtils;

    @Autowired
    ConfigUtils configUtils;

    private Map<String,Object> apDetail;
    MapSqlParameterSource params = null;

    public void check(long tmpId) throws StopFactorException {
        apDetail = dataUtils.getTmpApplication(tmpId);
        params = new MapSqlParameterSource();
        params.addValue("contactId", apDetail.get("contact_id"));
        params.addValue("tmpId", apDetail.get("tmp_id"));
        checkApplicationGate();
        checkActiveApplication();
        checkActiveAgreement();
    }

    // check for last applied
    private void checkApplicationGate() throws StopFactorException {
        Integer allowedAfterDays = Integer.parseInt(configUtils.getConfig("stop.factor.allowed.reapply.days"));
        params.addValue("allowedAfterDays", allowedAfterDays);
        String sql = " SELECT count(*) FROM _application_gate WHERE contact_id=:contactId AND tmp_id!=:tmpId AND ins_on>=SUBDATE(NOW(), INTERVAL :allowedAfterDays DAY) AND on_process=0 ";
        Integer found = namedJdbcTemplate.queryForObject(sql, params, Integer.class);
        if (found > 0) throw new StopFactorException("Another application exists during allowed re-apply days period", ""+apDetail.get("mobile"), DataUtils.STATUS_GATE_REJECTED_ALLOWED_MINIMUM_REAPPLY_DAYS);
    }

    // check for active application
    private void checkActiveApplication() throws StopFactorException {
        String sql = " SELECT count(*) FROM _application WHERE contact_id=:contactId AND on_process=1 ";
        Integer found = namedJdbcTemplate.queryForObject(sql, params, Integer.class);
        if (found > 0) throw new StopFactorException("Another application is in process", ""+apDetail.get("mobile"), DataUtils.STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_APPLICATION);
    }

    // check for active application
    private void checkActiveAgreement() throws StopFactorException {
        String sql = " SELECT count(*) FROM _contract WHERE contact_id=:contactId AND is_active=1 ";
        Integer found = namedJdbcTemplate.queryForObject(sql, params, Integer.class);
        if (found > 0) throw new StopFactorException("Another agreement is active", ""+apDetail.get("mobile"), DataUtils.STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_AGREEMENT);
    }

}
