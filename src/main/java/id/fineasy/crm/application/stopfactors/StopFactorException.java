package id.fineasy.crm.application.stopfactors;

public class StopFactorException extends Exception {
    private final int statusId;
    private final String mobile;


    public StopFactorException(String errorMessage, int statusId) {
        super(errorMessage);
        this.statusId = statusId;
        mobile = null;
    }

    public StopFactorException(String errorMessage, String mobile, int statusId) {
        super(errorMessage);
        this.statusId = statusId;
        this.mobile = mobile;
    }

    public int getStatusId() {
        return statusId;
    }
    public String getMobile() { return mobile; }
}
