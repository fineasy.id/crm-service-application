package id.fineasy.crm.application.stopfactors;

import id.fineasy.crm.application.component.ConfigUtils;
import id.fineasy.crm.application.component.DataUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@Component
public class EasypayMemberValidation {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    ConfigUtils configUtils;

    private Map<String,Object> apDetail;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void check(long tmpId) throws StopFactorException {
        apDetail = dataUtils.getTmpApplication(tmpId);
        if (apDetail.get("easypay_id")!=null) {
            log.info("Check easypay member mobile: "+apDetail.get("easypay_id"));
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("easypayId", apDetail.get("easypay_id"));
            String sql = " SELECT * FROM easypay_member WHERE easypay_id=:easypayId ";
            List<Map<String,Object>> results = namedJdbcTemplate.queryForList(sql, params);
            if (results.size()<=0) {
                // not easypay member
                throw new StopFactorException("Non easypay member!", (apDetail.get("mobile")==null)?null:""+apDetail.get("mobile"), DataUtils.STATUS_GATE_REJECTED_NON_EASYPAY_MEMBER);
            } else if (apDetail.get("contact_id")!=null) {
                Long contactId = (long) apDetail.get("contact_id");
                Long turnover = (long) results.get(0).get("turnover");
                params.addValue("contactId", contactId);
                params.addValue("turnover", results.get(0).get("turnover"));
                params.addValue("mobile", results.get(0).get("mobile"));
                log.info("Updating turnover "+results.get(0).get("turnover"));
                sql = " UPDATE contact SET monthly_turnover = :turnover, mobile=:mobile WHERE contact_id=:contactId ";
                namedJdbcTemplate.update(sql, params);
            }
        }
        else if (apDetail.get("mobile")!=null) {
            log.info("Check easypay member mobile: "+apDetail.get("mobile"));
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("mobile", apDetail.get("mobile"));
            String sql = " SELECT * FROM easypay_member WHERE mobile=:mobile ";
            List<Map<String,Object>> results = namedJdbcTemplate.queryForList(sql, params);
            if (results.size()<=0) {
                // not easypay member
                throw new StopFactorException("Non easypay member!", (apDetail.get("mobile")==null)?null:""+apDetail.get("mobile"), DataUtils.STATUS_GATE_REJECTED_NON_EASYPAY_MEMBER);
            } else if (apDetail.get("contact_id")!=null) {
                Long contactId = (long) apDetail.get("contact_id");
                Long turnover = (long) results.get(0).get("turnover");
                params.addValue("contactId", contactId);
                params.addValue("turnover", results.get(0).get("turnover"));
                params.addValue("easypayId", results.get(0).get("easypayId"));
                log.info("Updating turnover "+results.get(0).get("turnover"));
                sql = " UPDATE contact SET monthly_turnover = :turnover,easypay_id=:easypayId WHERE contact_id=:contactId ";
                namedJdbcTemplate.update(sql, params);
            }

        }
    }
}
