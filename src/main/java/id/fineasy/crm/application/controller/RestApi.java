package id.fineasy.crm.application.controller;

import id.fineasy.crm.application.component.ConfigUtils;
import id.fineasy.crm.application.component.DataUtils;
import id.fineasy.crm.application.component.MessageSender;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_NEW_APPLICATION_GATE;

@RestController
public class RestApi {
    @Autowired
    MessageSender messageSender;
    @Autowired
    ConfigUtils cfg;
    @Autowired
    DataUtils du;

    @RequestMapping(value = {"/gate/{id}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> gate(HttpServletRequest request, @PathVariable("id") Integer tmpId) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);
        jsonResult.put("tmpId", tmpId);

        messageSender.send(QUEUE_NEW_APPLICATION_GATE, jsonResult);

        return defaultReturn(jsonResult);
    }

    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

}
