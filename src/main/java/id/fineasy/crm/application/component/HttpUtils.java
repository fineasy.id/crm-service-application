package id.fineasy.crm.application.component;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Map;

public class HttpUtils {
    private final static PoolingHttpClientConnectionManager httpConnManager;
    private final static CloseableHttpClient httpClient;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    static {
        try {
            log.info("Init HTTP client ... ");
            httpConnManager = new PoolingHttpClientConnectionManager();
            httpConnManager.setMaxTotal(1000);
            httpConnManager.setDefaultMaxPerRoute(100);
            httpConnManager.setValidateAfterInactivity(30000);

            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(30000)
                    .setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000)
                    .build();

            HttpClientBuilder builder = HttpClients.custom()
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultRequestConfig(defaultRequestConfig)
                    .setConnectionManager(httpConnManager);

            httpClient = builder.build();
        } catch (Exception e) {
            log.error("Error: "+e,e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static CloseableHttpClient getHttpClient() {
        return httpClient;
    }

    public static PoolingHttpClientConnectionManager getConnectionManager() {
        return httpConnManager;
    }

    public static int getPendingConnection() {
        return HttpUtils.getConnectionManager().getTotalStats().getPending();
    }

    public static String get(String url, Map<String,Object> params) throws Exception {
        URIBuilder urlBuilder = new URIBuilder(url);

        for (String key : params.keySet()) {
            urlBuilder.addParameter(key, ""+params.get(key));
        }
        String sendUrl = urlBuilder.build().toString();
        log.info("URL: "+sendUrl);

        HttpGet httpget = new HttpGet(sendUrl);
        CloseableHttpResponse response = HttpUtils.getHttpClient().execute(httpget);
        HttpEntity entity = response.getEntity();
        log.info("Response status: "+response.getStatusLine().getStatusCode()+", "+response.getStatusLine().getReasonPhrase());

        byte[] bytes = EntityUtils.toByteArray(entity);
        String respText = new String(bytes);
        return respText;
    }

    public static String postJson(String url, JSONObject jsonObject) {
        try {
            log.info("Post JSON to: "+url);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new StringEntity(jsonObject.toString(4)));
            CloseableHttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            byte[] bytes = EntityUtils.toByteArray(entity);
            String respText = new String(bytes);
            log.info("Post JSON response: "+respText);
            return respText;
        } catch (Exception e) {
            log.error("Error while posting JSON: "+e);
            return null;
        }
    }
}
