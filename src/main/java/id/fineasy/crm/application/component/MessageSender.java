package id.fineasy.crm.application.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;

import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;
import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_NEW_CONTRACT;
import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT;

@Service
public class MessageSender {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private JmsTemplate jmsTemplate;


    public void send(String destination, JSONObject jsonReq) {
        jmsTemplate.convertAndSend(destination, jsonReq.toString());
    }

    public void send(String destination, String keyParam, Object valueParam) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(keyParam, valueParam);
        this.send(destination, jsonObject);
    }
}
