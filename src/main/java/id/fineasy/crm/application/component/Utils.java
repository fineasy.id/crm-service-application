package id.fineasy.crm.application.component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Utils {
    public static DecimalFormat getNumberFormat() {
        Locale locale = new Locale("id", "ID");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        String numberPattern = "#,##0";
        DecimalFormat decimalFormat = new DecimalFormat(numberPattern, symbols);
        return decimalFormat;
    }
}
