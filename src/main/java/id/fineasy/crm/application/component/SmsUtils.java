package id.fineasy.crm.application.component;

import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_SMS;

@Component
public class SmsUtils {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    private ConfigUtils configUtils;
    @Autowired
    private DataUtils dataUtils;
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    MessageSender messageSender;

    public void send(String template, String mobile, Map<String,Object> messageParams, String ref) throws Exception {
        try {
            Map<String,Object> httpParams = new HashMap<>();
            httpParams.put("mobile", mobile);
            StrSubstitutor sub = new StrSubstitutor(messageParams);
            String content = sub.replace(getMessageTemplate(template));

            JSONObject jsonReq = new JSONObject();
            jsonReq.put("content", content);
            jsonReq.put("mobile", mobile);
            jsonReq.put("tag", template);
            jsonReq.put("ref", ref);

            messageSender.send(QUEUE_SMS, jsonReq);

        } catch (Exception e) {
            log.error("Error sending SMS: "+e);
        }
    }

    public void send(String template, String mobile) throws Exception {
        send(template, mobile, new HashMap<>(), UUID.randomUUID().toString());

    }

    public String getMessageTemplate(String template) throws Exception {
        log.debug("get template: "+template);
        MapSqlParameterSource params = dataUtils.newSqlParams();
        params.addValue("tag", template);
        String sql = " SELECT content FROM sms_template WHERE tag=:tag ";
        String content = namedJdbcTemplate.queryForObject(sql, params, String.class);
        return content;
    }
}
