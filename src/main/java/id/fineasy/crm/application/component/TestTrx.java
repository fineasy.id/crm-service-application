package id.fineasy.crm.application.component;

import id.fineasy.crm.application.stopfactors.Deduplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.invoke.MethodHandles;

@Component
public class TestTrx {

    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;

    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    Deduplication deduplication;

    @Autowired
    DataUtils dataUtils;

    @Autowired
    DataSourceTransactionManager trxManager;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Transactional(rollbackFor = {Exception.class})
    public void test() throws Exception {
        log.info("START TRANSACTION!!!!!!!!!!!!!!!!!!!!");
        String sql = " INSERT INTO  test1 (name) VALUES ('indrakeren') ";
        namedJdbcTemplate.update(sql, new MapSqlParameterSource());
        if (true) throw new Exception("xxxx");
    }
}
