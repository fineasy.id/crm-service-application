package id.fineasy.crm.application.worker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Workers {
    @Autowired
    ApplicationIncomplete applicationIncomplete;

    public void start(ApplicationContext context) {
        applicationIncomplete.start();
    }
}
