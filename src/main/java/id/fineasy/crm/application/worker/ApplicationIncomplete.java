package id.fineasy.crm.application.worker;

import id.fineasy.crm.application.component.ConfigUtils;
import id.fineasy.crm.application.component.DataUtils;
import id.fineasy.crm.application.component.MessageSender;
import id.fineasy.crm.application.component.SmsUtils;
import id.fineasy.crm.application.stopfactors.Deduplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_NEW_APPLICATION_GATE;

@Component
public class ApplicationIncomplete {
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    Deduplication deduplication;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    DataSourceTransactionManager trxManager;
    @Autowired
    ConfigUtils configUtils;
    @Autowired
    SmsUtils smsUtils;
    @Autowired
    MessageSender messageSender;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void start() {
        taskScheduler.scheduleWithFixedDelay(new ApplicationIncompleteWorker(), 60000);
    }

    private class ApplicationIncompleteWorker implements Runnable {

        @Override
        public void run() {

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("statusId", DataUtils.STATUS_NEW);
            Long ttl = Long.parseLong(configUtils.getConfig("lost.app.after.minute"));
            log.info("Get incomplete application .... ttl: "+ttl);
            params.addValue("ttl", ttl);

            String sql = " SELECT tmp_id FROM `application_tmp` WHERE 1 " +
                    " AND is_processed=0 AND is_finished=0 AND process_end<SUBDATE(NOW(), INTERVAL :ttl MINUTE) " +
                    " AND status_id=:statusId "+
                    " ORDER BY process_end LIMIT 0,100 ";
            List<Long> ids = namedJdbcTemplate.queryForList(sql, params, Long.class);
            for (Long tmpId : ids) {
                log.info("Processing incomplete application ID#: "+tmpId);
                params.addValue("tmpId", tmpId);
                params.addValue("statusId", DataUtils.STATUS_GATE_ON_PROCESS);
                sql = " UPDATE application_tmp SET status_id=:statusId WHERE tmp_id=:tmpId ";
                namedJdbcTemplate.update(sql, params);

                messageSender.send(QUEUE_NEW_APPLICATION_GATE, "tmpId", tmpId);
            }
        }
    }
}
