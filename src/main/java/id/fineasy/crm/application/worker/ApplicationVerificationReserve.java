package id.fineasy.crm.application.worker;

import id.fineasy.crm.application.component.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.application.component.DataUtils.STATUS_APPLICATION_ON_VERIFICATION;
import static id.fineasy.crm.application.component.DataUtils.STATUS_APPLICATION_READY_FOR_VERIFICATION;

public class ApplicationVerificationReserve implements Runnable {
    final ApplicationContext context;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DataSourceTransactionManager trxManager;
    private final ConfigUtils cfg;

    public ApplicationVerificationReserve(ApplicationContext context) {
        this.context = context;
        jdbcTemplate = (NamedParameterJdbcTemplate) context.getBean("namedJdbcTemplate");
        this.trxManager = (DataSourceTransactionManager) context.getBean("trxManager");
        this.cfg = (ConfigUtils) context.getBean("configUtils");
    }

    @Override
    public void run() {

        int reservedAppTtl = Integer.parseInt(cfg.getConfig("reserved.application.ttl"));
        log.debug("Find reserved application with TTL : "+reservedAppTtl+" minutes");

        TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
        trxTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        trxTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    MapSqlParameterSource params = new MapSqlParameterSource();
                    params.addValue("ttl", reservedAppTtl);
                    String sql = " SELECT * FROM `application_verification_reserve` WHERE 1 AND ADDDATE(ins_on, INTERVAL :ttl MINUTE)<NOW() ";
                    List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);

                    if (rows.size() > 0) {
                        log.info("Found "+rows.size()+" expired reserved applications");
                        for (Map<String,Object> row: rows) {
                            log.info("Send back to queue, application# "+row.get("app_id"));
                            params.addValue("appId", row.get("app_id"));
                            params.addValue("newStatusId", STATUS_APPLICATION_READY_FOR_VERIFICATION);
                            params.addValue("oldStatusId", STATUS_APPLICATION_ON_VERIFICATION);
                            sql = " UPDATE application SET status_id=:newStatusId WHERE app_id=:appId AND status_id=:oldStatusId ";
                            jdbcTemplate.update(sql, params);

                            sql = " DELETE FROM application_verification_reserve WHERE app_id=:appId ";
                            jdbcTemplate.update(sql, params);
                        }
                    }

                } catch (Exception e) {

                }
            }
        });
    }
}
