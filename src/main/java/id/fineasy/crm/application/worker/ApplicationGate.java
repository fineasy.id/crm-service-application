package id.fineasy.crm.application.worker;

import id.fineasy.crm.application.component.ConfigUtils;
import id.fineasy.crm.application.component.DataUtils;
import id.fineasy.crm.application.component.SmsUtils;
import id.fineasy.crm.application.stopfactors.Deduplication;
import id.fineasy.crm.application.stopfactors.EasypayMemberValidation;
import id.fineasy.crm.application.stopfactors.StopFactorException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.application.component.DataUtils.CHANNEL_WEBSITE_REPEAT;
import static id.fineasy.crm.application.component.DataUtils.STATUS_GATE_REJECTED_NON_ELIGIBLE_REPEAT;
import static id.fineasy.crm.application.config.ActiveMQConfig.QUEUE_NEW_APPLICATION_GATE;

@Component
public class ApplicationGate {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    Deduplication deduplication;
    @Autowired
    EasypayMemberValidation memberValidation;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    DataSourceTransactionManager trxManager;
    @Autowired
    ConfigUtils configUtils;
    @Autowired
    SmsUtils smsUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_NEW_APPLICATION_GATE)
    public void recalculateContract(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonObject = new JSONObject(jsonObjectString);
            log.info("Application gate request received! "+jsonObject.toString());
            Long tmpId = jsonObject.getLong("tmpId");
            ApplicationGateWorker worker = new ApplicationGateWorker(tmpId);
            worker.run();
        } catch (Exception e) {
            log.error("Error start async contract activation: "+e,e);
        }
    }

    private class ApplicationGateWorker {
        Long contactId = null;
        String mobile = null;
        final Long tmpId;
        private Map<String,Object> apDetail;



        boolean PROCESS_SF_DEDUPLICATION = true;

        public ApplicationGateWorker(Long tmpId) {
            this.tmpId = tmpId;
        }

        public void run() {
            log.debug("Start the worker now ....!");
            getQueue();
        }

        private void getQueue() {
            String sql = " SELECT tmp_id FROM `application_tmp` WHERE 1 " +
                    " AND is_processed=0 AND (is_finished=1 OR (is_finished=0 AND process_end<SUBDATE(NOW(), INTERVAL "+configUtils.getConfig("lost.app.after.minute")+" MINUTE))) " +
                    " AND status_id IN (251,254) "+
                    " AND tmp_id=:tmpId "+
                    " ORDER BY process_end LIMIT 0,100 ";

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("statusId", DataUtils.STATUS_NEW);
            params.addValue("tmpId", tmpId);

            List<Map<String,Object>> queryResult = namedJdbcTemplate.queryForList(sql, params);
            if (queryResult.size()<=0) {
                log.warn("Application #"+tmpId+" is not valid application gate!");
            }
            for (Map<String,Object> ap : queryResult) {


                TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
                trxTemplate.execute(new TransactionCallback() {

                    @Override
                    public Object doInTransaction(TransactionStatus status) {
                        StopFactorException stopFactorException = null;
                        Exception processException = null;
                        try {
                            process(ap);
                        }
                        catch (StopFactorException e) {
                            stopFactorException = e;
                        }
                        catch (Exception e) {
                            status.setRollbackOnly();
                            processException = e;
                        }
                        return processReturn(ap, stopFactorException, processException);
                    }
                });


            }
        }

        private Object processReturn(Map<String,Object> ap, StopFactorException stopFactorException, Exception processException) {
            if (stopFactorException!=null) {
                try {
                    log.error("Stop factor catched! "+stopFactorException+", status ID: "+stopFactorException.getStatusId());
                    dataUtils.updateTmpApplicationStatusAndProcessed(Long.parseLong(""+ap.get("tmp_id")), stopFactorException.getStatusId());
                    // send SMS rejection
                    if (stopFactorException.getMobile()!=null) {
                        try {
                            smsUtils.send("REJECTION", stopFactorException.getMobile());
                        } catch (Exception e2) { log.error ("Error sending sms: "+e2); }
                    }
                } catch (Exception ex) {
                    log.error("Error process stop factor: "+ex, ex);
                }
            } else if (processException!=null) {
                log.error("Error while process application: "+processException,processException);
                dataUtils.updateTmpApplicationStatusAndProcessed((Long) ap.get("tmp_id"), DataUtils.STATUS_GATE_UNEXPECTED_ERROR);
            }
            return null;
        }

        protected void process(Map<String,Object> ap) throws StopFactorException, Exception {
            apDetail = dataUtils.getTmpApplication(tmpId);

            contactId = (Long) apDetail.get("contact_id");
            mobile = (String) apDetail.get("mobile");
            if (mobile == null) {
                throw new StopFactorException("[APP#"+tmpId+"] Mobile phone is not suplied!", DataUtils.STATUS_REJECTED_MOBILE_EMPTY);
            }

            log.info("[APP#"+tmpId+"] Processing #"+tmpId+", contact id #"+contactId);

            checkAndCreateContact();

            memberValidation.check(tmpId);
            if (PROCESS_SF_DEDUPLICATION) deduplication.check(tmpId);

            Boolean isFinished = (Boolean) apDetail.get("is_finished");

            if (isFinished)
                moveToVerification(tmpId, dataUtils.CHANNEL_WEBSITE);
            else if (mobile!=null)
                moveToVerification(tmpId, dataUtils.CHANNEL_WEBSITE_INCOMPLETE);
        }

        private void checkAndCreateContact() {
            if (contactId == null) {
                // contact doesn't exists skipping deduplication stop factor check ...
                PROCESS_SF_DEDUPLICATION = false;
                String sql = " INSERT INTO contact (first_name,mid_name,last_name,full_name,mobile,email,reg_id,birth_date,mother_name,home_addr,business_addr,business_years_id,easypay_id,education_id,marital_status_id,children_id,ref_id) " +
                        " SELECT first_name,mid_name,last_name,full_name,mobile,email,reg_id,birth_date,mother_name,home_addr,business_addr,business_years_id,easypay_id,education_id,marital_status_id,children_id,ref_id FROM application_tmp WHERE tmp_id=:tmpId ";
                MapSqlParameterSource params = new MapSqlParameterSource();
                params.addValue("tmpId", tmpId);

                KeyHolder holder = new GeneratedKeyHolder();
                namedJdbcTemplate.update(sql, params, holder);
                contactId = holder.getKey().longValue();
                log.info("[APP#"+tmpId+"] contact not found, inserted contact ID: "+contactId);
            } else {
                log.info("[APP#"+tmpId+"] contact found!");
            }
        }

        public void moveToVerification(long tmpId, long channelId) throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();

            // check if this customer is repeat loan ...
            boolean isRepeatCustomer = false;
            try {
                isRepeatCustomer = isRepeatedCustomer();
                if (isRepeatCustomer) {
                    channelId = CHANNEL_WEBSITE_REPEAT;
                }
            } catch (Exception e) {
                log.error("Error while moving to application queue: "+e);
                return;
            }

            params.addValue("tmpId", tmpId);
            params.addValue("channelId", channelId);
            params.addValue("systemUserId", DataUtils.USER_ID_SYSTEM);

            String sql = " INSERT INTO application (channel_id,ins_by,mod_by,tmp_id,contact_id,product_id,loan_seq,purpose_id,amount_req,term_req) " +
                    " SELECT :channelId,:systemUserId,:systemUserId,tmp_id,contact_id,product_id,loan_seq,purpose_id,loan_amount,loan_term FROM _application_gate WHERE tmp_id=:tmpId ";
            KeyHolder holder = new GeneratedKeyHolder();
            namedJdbcTemplate.update(sql, params, holder);
            Long appId = holder.getKey().longValue();

            params.addValue("statusId", DataUtils.STATUS_GATE_PASSED_VERIFICATION);
            sql = " UPDATE application_tmp SET status_id=:statusId, is_processed=1,processed_on=NOW() WHERE tmp_id=:tmpId ";
            namedJdbcTemplate.update(sql, params);

            log.info("Move to verification with application ID: "+appId);
            try {
                smsUtils.send("APPLICATION1", "" + apDetail.get("mobile"));
            } catch (Exception e2) { log.error ("Error sending sms: "+e2); }
        }

        private boolean isRepeatedCustomer() throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contactId", contactId);
            params.addValue("tmpId", tmpId);
            String sql = " SELECT contract_closed FROM _contact WHERE contact_id=:contactId ";
            Long closedContract = dataUtils.getJT().queryForObject(sql, params, Long.class);
            if (closedContract > 0) {
                Long loanSeq = closedContract+1;
                log.info("This customer has closed contract before, check if eligible to receive the repeat loan!");

                sql = " SELECT count(*) FROM _contact_eligible_repeat WHERE contact_id=:contactId ";
                Integer found = dataUtils.getJT().queryForObject(sql, params, Integer.class);
                if (found > 0) {
                    params.addValue("loanSeq", loanSeq);
                    log.info("Contact #"+contactId+" is eligible to get repeat loan!");
                    sql = " UPDATE application_tmp SET loan_seq=:loanSeq WHERE tmp_id=:tmpId ";
                    dataUtils.getJT().update(sql, params);
                    return true;
                }
                else {
                    dataUtils.updateTmpApplicationStatusAndProcessed(tmpId, STATUS_GATE_REJECTED_NON_ELIGIBLE_REPEAT);
                    throw new Exception("Non eligible repeat loan customer!");
                }
            } else {
                log.info("Contact #"+contactId+" is eligible to get repeat loan!");
                return false;
            }
        }

        public void moveToTelesales(long tmpId) throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("tmpId", tmpId);
            params.addValue("contactId", contactId);
            params.addValue("systemUserId", DataUtils.USER_ID_SYSTEM);
            params.addValue("typeId", DataUtils.TELESALES_SOURCE_TYPE_INCOMPLETE_APPLICATION);
            params.addValue("statusId", DataUtils.STATUS_GATE_PASSED_TELESALES);

            String sql = " INSERT INTO telesales_queue (mobile,contact_id,type_id,application_tmp_id,ins_by,mod_by)  " +
                    " SELECT mobile,:contactId,:typeId,tmp_id,:systemUserId,:systemUserId FROM application_tmp WHERE tmp_id=:tmpId ";

            namedJdbcTemplate.update(sql, params);

            sql = " UPDATE application_tmp SET status_id=:statusId, is_processed=1,processed_on=NOW() WHERE tmp_id=:tmpId ";
            namedJdbcTemplate.update(sql, params);
        }


    }
}
