package id.fineasy.crm.application;

import id.fineasy.crm.application.worker.ApplicationIncomplete;
import id.fineasy.crm.application.worker.ApplicationVerificationReserve;
import id.fineasy.crm.application.worker.Workers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class ServiceRunner implements ApplicationRunner {
    @Autowired
    Workers workers;

    @Autowired
    private ApplicationContext context;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("Start runner!");
        workers.start(context);
        taskScheduler.scheduleWithFixedDelay(new ApplicationVerificationReserve(context), 60000);
    }

}
